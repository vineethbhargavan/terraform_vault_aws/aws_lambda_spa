const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const https = require('https');
const os = require('os');

module.exports.handler = async (event) => {

  const s3BucketName = process.env.s3bucket;
  const imageFileName = process.env.imageName;

  // Get a pre-signed URL for the S3 object
  const params = {
    Bucket: s3BucketName,
    Key: imageFileName,
    Expires: 60, // URL expiration time in seconds
  };

  const imageUrl = await s3.getSignedUrlPromise('getObject', params);
  console.log('Signed imageUrl: ', imageUrl);
  let ipAddress;
  const networkInterfaces = os.networkInterfaces();
  Object.keys(networkInterfaces).forEach((key) => {
    const iface = networkInterfaces[key];
    iface.forEach((entry) => {
      console.log(`IP : ${entry.address}`)
      if (!entry.internal && entry.family === 'IPv4') {
        ipAddress = entry.address;
      }
    });
  });
  //const ipAddress = await getPublicIpAddress();
  console.log('ipAddress: ', ipAddress);

  console.log('Event: ', event);
  let responseMessage = 'OK';
  const htmlContent = `
    <html>
        <head>
            <title>Lambda Image Renderer</title>
        </head>
        <body>
            <h1>Date on Webserver IP Address ${ipAddress} is ${new Date().toISOString()}</h1>
            <img src="${imageUrl}" alt="S3 Image"/>
        </body>
    </html>
`;
  return {
    statusCode: 200,
    body: htmlContent,
    headers: {
      'Content-Type': 'text/html',
    }
  }
}

