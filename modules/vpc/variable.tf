variable "environment" {
  type        = string
  default     = "dev"
}

variable "project" {
  type        = string
  default     = "webapp-ok"
}

variable "aws_region" {
  type        = string
  default     = "ap-southeast-2"
  description = "AWS region for all resources"
}