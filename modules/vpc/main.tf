# Create a VPC
resource "aws_vpc" "project_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "${var.environment}-vpc"
  }
}

resource "aws_internet_gateway" "project_igw" {
  vpc_id = aws_vpc.project_vpc.id

  tags = {
    Name = "${var.environment}-igw"
  }
}


# Create an Elastic IP for the NAT Gateway
resource "aws_eip" "project_eip" {}

# Create public and private subnets
resource "aws_subnet" "project_subnet_public" {
  vpc_id                  = aws_vpc.project_vpc.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "${var.aws_region}a"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.environment}-subnet-public"
  }
}

resource "aws_subnet" "project_subnet_private" {
  vpc_id                  = aws_vpc.project_vpc.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "${var.aws_region}b"
  map_public_ip_on_launch = false
  tags = {
    Name = "${var.environment}-subnet-private"
  }
}

# Create a NAT Gateway in the public subnet
resource "aws_nat_gateway" "project_nat_gateway" {
  depends_on = [aws_internet_gateway.project_igw]
  allocation_id = aws_eip.project_eip.id
  subnet_id     = aws_subnet.project_subnet_public.id
}

# Create a route table for the private subnet
resource "aws_route_table" "project_private_route_table" {
  vpc_id = aws_vpc.project_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.project_nat_gateway.id
  }
}

# Create a route table for public subnet
resource "aws_route_table" "project_public_route_table" {
  vpc_id = aws_vpc.project_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.project_igw.id
  }
}

# Associate public subnet with public route table
resource "aws_route_table_association" "project_public_route_association" {
  subnet_id      = aws_subnet.project_subnet_public.id
  route_table_id = aws_route_table.project_public_route_table.id
}

# Associate the private route table with the private subnet
resource "aws_route_table_association" "project_private_route_association" {
  subnet_id      = aws_subnet.project_subnet_private.id
  route_table_id = aws_route_table.project_private_route_table.id
}

# Create a security group allowing outbound internet traffic
resource "aws_security_group" "project_security_group" {
  vpc_id = aws_vpc.project_vpc.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.environment}-security-group"
  }
}





# # Create Lambda function
# resource "aws_lambda_function" "project_lambda" {
#   function_name    = "projectLambdaFunction"
#   handler          = "index.handler"
#   runtime          = "nodejs14.x"
#   source_code_hash = filebase64("${path.module}/lambda_function.zip")
#   role             = aws_iam_role.lambda_role.arn
#   vpc_config {
#     subnet_ids         = [aws_subnet.project_subnet_private.id]
#     security_group_ids = [aws_security_group.project_security_group.id]
#   }

#   # ... other lambda configuration ...
# }

# # Create IAM role for Lambda function
# resource "aws_iam_role" "lambda_role" {
#   name = "LambdaVpcRole"

#   assume_role_policy = jsonencode({
#     Version = "2012-10-17",
#     Statement = [
#       {
#         Action = "sts:AssumeRole",
#         Effect = "Allow",
#         Principal = {
#           Service = "lambda.amazonaws.com",
#         },
#       },
#     ],
#   })
# }

# # Attach policies to IAM role as needed
# resource "aws_iam_role_policy_attachment" "lambda_role_attachment" {
#   policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
#   role       = aws_iam_role.lambda_role.name
# }

# # Additional policies can be attached based on your requirements
