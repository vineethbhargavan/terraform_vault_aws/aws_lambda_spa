output "private_subnet_id" {
  description = "Private subnet ID"
  value = aws_subnet.project_subnet_private.id
}

output "lambda_security_group" {
  description = "Name of the S3 bucket used to store function code."
  value = aws_security_group.project_security_group.id
}

