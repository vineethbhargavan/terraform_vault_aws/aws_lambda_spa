variable "lambda_arn" {
  description = "lambda_arn"
}

variable "lambda_name" {
  description = "lambda_name"
}

variable "environment" {
  description = "environment"
  default= "dev"
}

variable "project" {
  type        = string
  default     = "webapp-ok"
}