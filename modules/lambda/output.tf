output "function_name" {
  description = "Name of the Lambda function."
  value = aws_lambda_function.app_OK.function_name
}

output "lambda_arn" {
  description = "cloudwatch_arn"
  value = aws_lambda_function.app_OK.invoke_arn
}

output "lambda_name" {
  description = "cloudwatch_arn"
  value = aws_lambda_function.app_OK.function_name
}