# data "archive_file" "lambda_OK" {
#   type = "zip"
#   source_dir  = "../${path.module}/nodeapp"
#   output_path = "../${path.module}/nodeapp.zip"
# }

# resource "null_resource" "check_zip_file" {
#   depends_on = [data.archive_file.lambda_OK]    
#   provisioner "local-exec" {
#     command = "pwd && ls -lrt"
#   }
# }

# resource "aws_s3_object" "lambda_OK" {
#   bucket = var.aws_bucket_id
#   depends_on = [null_resource.check_zip_file]
#   key    = "nodeapp.zip"
#   source = data.archive_file.lambda_OK.output_path
#   etag = filemd5(data.archive_file.lambda_OK.output_path)
# }


resource "aws_lambda_function" "app_OK" {
  function_name = "${var.environment}-app-ok"

  s3_bucket = var.aws_bucket_id
  s3_key    = var.s3_key

  runtime = "nodejs14.x"
  handler = "app.handler"

  source_code_hash = var.source_code_hash
  environment {
    variables = {
      s3bucket  = var.aws_bucket_id,
      imageName = var.logo,
    }
  }
  vpc_config {
    subnet_ids         = [var.project_subnet_private_id]
    security_group_ids = [var.project_security_group_id]
  }
  role = aws_iam_role.lambda_exec.arn
  tags = {
    Project = "${var.environment}-${var.project}"
  }
}

resource "aws_cloudwatch_log_group" "app_OK" {
  name              = "/aws/lambda/${aws_lambda_function.app_OK.function_name}"
  retention_in_days = 30
  tags = {
    Project = "${var.environment}-${var.project}"
  }
}

resource "aws_iam_role" "lambda_exec" {
  name = "${var.environment}-serverless_lambda"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda_exec.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

# role to read from s3:
# IAM policy for S3 read access
resource "aws_iam_policy" "s3_read_policy" {
  name        = "S3ReadPolicy"
  description = "Policy for Lambda to read from S3 bucket"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action   = "s3:GetObject",
        Effect   = "Allow",
        Resource = "arn:aws:s3:::${var.aws_bucket_id}/*",
      },
    ],
  })
}


# Attach the S3 read policy to the Lambda role
resource "aws_iam_role_policy_attachment" "attach_s3_policy" {
  policy_arn = aws_iam_policy.s3_read_policy.arn
  role       = aws_iam_role.lambda_exec.name
}


# Additional policies for Lambda to access VPC resources
resource "aws_iam_policy" "lambda_vpc_policy" {
  name        = "LambdaVpcPolicy"
  description = "Policy for Lambda to access VPC resources"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action   = "ec2:CreateNetworkInterface",
        Effect   = "Allow",
        Resource = "*",
      },
      {
        Action   = "ec2:DescribeNetworkInterfaces",
        Effect   = "Allow",
        Resource = "*",
      },
      {
        Action   = "ec2:DeleteNetworkInterface",
        Effect   = "Allow",
        Resource = "*",
      },
    ],
  })
}

resource "aws_iam_role_policy_attachment" "lambda_vpc_policy_attachment" {
  policy_arn = aws_iam_policy.lambda_vpc_policy.arn
  role       = aws_iam_role.lambda_exec.name
}
