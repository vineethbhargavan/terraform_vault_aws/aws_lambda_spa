variable "aws_bucket_id" {
  type        = string
  description = "bucket id"
}

variable "environment" {
  description = "environment"
  default= "dev"
}

variable "source_code_hash" {
  description = "source_code_hash"
}

variable "s3_key" {
  description = "s3_key"
}

variable "project" {
  type        = string
  default     = "webapp-ok"
}

variable "logo" {
  type        = string
}
variable "project_subnet_private_id" {
  type        = string
}
variable "project_security_group_id" {
  type        = string
}