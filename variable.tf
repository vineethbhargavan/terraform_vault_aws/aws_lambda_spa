variable "environment" {
  description = "environment"
  default= "devsbs"
}

variable "aws_region" {
  type        = string
  default     = "ap-southeast-2"
  description = "AWS region for all resources"
}