terraform {
  backend "s3" {
    bucket = "devsbs-terraformstatefile"
    key    = "terraform.tfstate"
    region = "ap-southeast-2"
  }
}

# terraform {
#   backend "remote" {
#     hostname = "app.terraform.io"
#     organization = "vineethworkspace"

#     workspaces {
#       name = "aws_lambda_spa"
#     }
#   }
# }

provider "aws" {
  region = var.aws_region
}

# Requirement is https://<url>/hello
# Simple EC2 server
# module "setup_webserver_ec2" {
#     source = "./modules/ec2"
#     html_content = "OK"
#     ec2_key = "ubuntu16_vb"
# }


module "setup_s3_tostore_lambda" {
  source      = "./modules/s3"
  environment = var.environment
}


data "archive_file" "lambda_OK" {
  depends_on  = [module.setup_s3_tostore_lambda]
  type        = "zip"
  source_dir  = "${path.module}/nodeapp"
  output_path = "${path.module}/nodeapp.zip"
}

resource "aws_s3_object" "lambda_OK" {
  depends_on = [data.archive_file.lambda_OK]
  bucket     = module.setup_s3_tostore_lambda.lambda_bucket_name
  key        = "nodeapp.zip"
  source     = data.archive_file.lambda_OK.output_path
  etag       = filemd5(data.archive_file.lambda_OK.output_path)
}


resource "aws_s3_object" "lambda_Image" {
  bucket = module.setup_s3_tostore_lambda.lambda_bucket_name
  key    = "sbs-world-cup.jpeg"
  source = "${path.module}/sbs-world-cup.jpeg"
  etag   = filemd5("${path.module}/sbs-world-cup.jpeg")
}
module "setup_vpc" {
  source      = "./modules/vpc"
  environment = var.environment
  depends_on  = [aws_s3_object.lambda_OK]
}

module "setup_lambda" {
  source           = "./modules/lambda"
  environment      = var.environment
  depends_on       = [module.setup_vpc]
  aws_bucket_id    = module.setup_s3_tostore_lambda.lambda_bucket_name
  source_code_hash = data.archive_file.lambda_OK.output_base64sha256
  s3_key           = aws_s3_object.lambda_OK.key
  logo             = aws_s3_object.lambda_Image.key
  project_subnet_private_id = module.setup_vpc.private_subnet_id
  project_security_group_id = module.setup_vpc.lambda_security_group
}

module "setup_apigw" {
  source      = "./modules/api"
  depends_on  = [module.setup_lambda]
  environment = var.environment
  lambda_arn  = module.setup_lambda.lambda_arn
  lambda_name = module.setup_lambda.lambda_name
}

output "base_url" {
  description = "Base URL for API Gateway stage."
  value       = module.setup_apigw.base_url
}
